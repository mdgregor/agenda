# Agenda Bill Calculator
### Usage
 Used to determine agenda bill due dates for the City of Burien 
##
### Build
Windows build created with [pyinstaller](https://ourcodeworld.com/articles/read/273/how-to-create-an-executable-exe-from-a-python-script-in-windows-using-pyinstaller)

Command: <code>pyinstaller agenda_bill_calculator.py --windowed</code>

Navigate to <code>/dist</code> folder for program executables

