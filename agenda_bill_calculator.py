#!/usr/bin/env python

"""
Copyright 2019 Michael Gregor

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import wx
import pytz
import enum
from wx.grid import GridStringTable
from datetime import timedelta
from datetime import datetime

TIMEZONE = pytz.timezone("America/Los_Angeles")


class StringValues(enum.Enum):
    PLANNING_COMMISSION_MEETING = "Planning Commission Meeting"
    AGENDA_BILL_DUE = "Initial Agenda Bill Due"
    FIRST_TOUCH = "First Presentation to Council"
    UPDATED_BILL_DUE = "Updated Agenda Bill Due"
    SECOND_TOUCH = "Second Presentation to Council"
    FINAL_UPDATE_BILL_DUE = "Final Updated Agenda Bill Due"
    SELECTED_MEETING = "Meeting Date for Final Approval"
    REQUIRED_DATES_PAST = "Required Dates have already past"
    MEETING_STRING = "Council Meeting: "
    STUDY_SESSION_STRING = "Study Session:      "
    PLANNING_COMM_STRING = "Planning Commission: "
    RESULTS_TIME_OUTPUT_FORMAT = "%A, %B %d, %Y"
    AVAILABLE_DATES_TIME_FORMAT = "%B %d, %Y"


DATE_TIMES_OPTIONS = [
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 1, 6, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 1, 8, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 1, 22, tzinfo=TIMEZONE)),
    (f"{StringValues.STUDY_SESSION_STRING.value}",  datetime(2020, 1, 27, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 2, 3, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 2, 12, tzinfo=TIMEZONE)),
    (f"{StringValues.STUDY_SESSION_STRING.value}",  datetime(2020, 2, 24, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 2, 26, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 3, 2, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 3, 11, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 3, 16, tzinfo=TIMEZONE)),
    (f"{StringValues.STUDY_SESSION_STRING.value}",  datetime(2020, 3, 23, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 3, 25, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 4, 6, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 4, 8, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 4, 20, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 4, 22, tzinfo=TIMEZONE)),
    (f"{StringValues.STUDY_SESSION_STRING.value}",  datetime(2020, 4, 27, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 5, 4, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 5, 13, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 5, 18, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 5, 27, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 6, 1, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 6, 10, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 6, 15, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 6, 24, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 7, 6, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 7, 8, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 7, 20, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 7, 22, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 8, 3, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 8, 12, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 8, 17, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 8, 26, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 9, 9, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 9, 21, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 9, 23, tzinfo=TIMEZONE)),
    (f"{StringValues.STUDY_SESSION_STRING.value}",  datetime(2020, 9, 28, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 10, 5, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 10, 14, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 10, 19, tzinfo=TIMEZONE)),
    (f"{StringValues.STUDY_SESSION_STRING.value}",  datetime(2020, 10, 26, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 10, 28, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 11, 2, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 11, 10, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 11, 16, tzinfo=TIMEZONE)),
    (f"{StringValues.STUDY_SESSION_STRING.value}",  datetime(2020, 11, 23, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 11, 24, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 12, 7, tzinfo=TIMEZONE)),
    (f"{StringValues.PLANNING_COMM_STRING.value}",  datetime(2020, 12, 9, tzinfo=TIMEZONE)),
    (f"{StringValues.MEETING_STRING.value}",        datetime(2020, 12, 21, tzinfo=TIMEZONE)),
    (f"{StringValues.STUDY_SESSION_STRING.value}",  datetime(2020, 12, 28, tzinfo=TIMEZONE)),
]

DATE_OPTIONS = []
DATE_TIMES = []
AVAILABLE_DATES = []

for date in DATE_TIMES_OPTIONS:
    DATE_OPTIONS.append(date[0])
    DATE_TIMES.append(date[1])
    if not date[0].startswith(StringValues.PLANNING_COMM_STRING.value):
        AVAILABLE_DATES.append(date[0] + date[1].strftime(StringValues.AVAILABLE_DATES_TIME_FORMAT.value))


class AgendaFrame(wx.Frame):

    def __init__(self, *args, **kwargs):
        super(AgendaFrame, self).__init__(*args, **kwargs)
        self.approval_needed = None
        self.public_hearing_needed = None
        self.planning_commission_needed = None
        self.meeting_date_picker = None
        self.approval = False
        self.public_hearing = False
        self.planning_commission = False

        panel = wx.Panel(self)

        self.make_date_picker(panel)

        self.make_checkboxes(panel)

        self.make_menu_bar()

        self.grid = SimpleGrid(self)

        self.Center()
        self.calculate_button = wx.Button(panel, label="Calculate", pos=(550, 40))
        self.Bind(wx.EVT_BUTTON, self.calculate_button_pressed, self.calculate_button)

    def make_menu_bar(self):
        file_menu = wx.Menu()
        exit_item = file_menu.Append(wx.ID_EXIT)

        help_menu = wx.Menu()
        help_item = help_menu.Append(wx.ID_HELP)

        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, "&File")
        menu_bar.Append(help_menu, "&Help")

        self.SetMenuBar(menu_bar)

        self.Bind(wx.EVT_MENU, self.on_exit, exit_item)
        self.Bind(wx.EVT_MENU, self.on_help, help_item)

    def on_exit(self, event):
        self.Close(True)

    def on_help(self, event):
        wx.MessageBox("Megan Gregor at MeganG@burienwa.gov",
                      "Contact",
                      wx.OK | wx.ICON_INFORMATION)

    def calculate_button_pressed(self, event):
        self.approval_needed_checked(self.approval_needed)
        self.public_hearing_needed_checked(self.public_hearing_needed)
        self.planning_commission_needed_checked(self.planning_commission_needed)

        if (self.public_hearing or self.planning_commission) and not self.approval:
            self.message_box_error(
                "'Agenda bill needs potential action' checkbox must be checked\nif `Needs Public Hearing' or 'Needs Planning Commission' is checked")
            return

        if self.planning_commission and self.public_hearing:
            self.message_box_error(
                "Only one of Needs Public Hearing or Needs Planning Commission can be checked"
            )
            return

        self.meeting_date_picker_selected(self.meeting_date_picker)

        event.Skip()

    def make_checkboxes(self, panel):
        wx.StaticText(panel, -1, pos=(300, 10)).SetLabelText("(Check if applicable)")
        self.approval_needed = wx.CheckBox(panel, id=-1, label='Agenda bill needs potential action', pos=(300, 30),
                                           size=(200, -1))

        self.public_hearing_needed = wx.CheckBox(panel, id=-1, label='Needs Public Hearing', pos=(300, 50),
                                                 size=(150, -1))

        self.planning_commission_needed = wx.CheckBox(panel, id=-1, label='Needs Planning Commission', pos=(300, 70),
                                                      size=(200, -1))

    def approval_needed_checked(self, event):
        self.approval = event.GetValue()

    def public_hearing_needed_checked(self, event):
        self.public_hearing = event.GetValue()

    def planning_commission_needed_checked(self, event):
        self.planning_commission = event.GetValue()

    def make_date_picker(self, panel):
        wx.StaticText(panel, -1, pos=(10, 10)).SetLabelText("Select Meeting for Final Approval")
        self.meeting_date_picker = wx.ListBox(panel, -1, pos=(10, 40), size=(275, 195), choices=AVAILABLE_DATES,
                                              style=wx.LB_SINGLE)

    def meeting_date_picker_selected(self, event):
        selected_date, agenda_bill_index = None, None
        date_string = event.GetStringSelection()
        if date_string == '':
            self.message_box_error("Please select a Meeting Date")
            return

        for index, item in enumerate(DATE_TIMES):
            if date_string.endswith(item.strftime(StringValues.AVAILABLE_DATES_TIME_FORMAT.value)):
                selected_date, agenda_bill_index = DATE_TIMES[index], index

        if not self.approval:

            self.grid.SetRowLabelValue(0, StringValues.AGENDA_BILL_DUE.value)

            agenda_bill_due_date = selected_date - timedelta(days=6)
            self.set_cell_values([agenda_bill_due_date], [1, 2, 3, 4, 5])

        else:
            if date_string.startswith(StringValues.STUDY_SESSION_STRING.value.strip()):
                self.message_box_error(
                    "Agenda bills that require action and/or a public hearing\ncan not be on Study Session dates")
                return

            labels_text = [
                StringValues.AGENDA_BILL_DUE.value,
                StringValues.FIRST_TOUCH.value,
                StringValues.UPDATED_BILL_DUE.value,
                StringValues.SECOND_TOUCH.value,
                StringValues.FINAL_UPDATE_BILL_DUE.value,
                StringValues.SELECTED_MEETING.value
            ]

            if self.public_hearing:
                self.set_labels(labels_text)
                first_index, second_index = self.get_previous_two_meeting_indexes(agenda_bill_index)

                try:
                    first_touch = DATE_TIMES[first_index]
                    second_touch = DATE_TIMES[second_index]
                    agenda_bill_due_date = first_touch - timedelta(days=6)
                except TypeError:
                    self.message_box_error(StringValues.REQUIRED_DATES_PAST.value)
                    return

                agenda_bill_update_date = second_touch - timedelta(days=6)
                agenda_bill_final_update = selected_date - timedelta(days=6)

                public_hearing_date_values = [agenda_bill_due_date, first_touch, agenda_bill_update_date,
                                              second_touch, agenda_bill_final_update, selected_date]

                self.set_cell_values(public_hearing_date_values, [])

            else:
                labels_text.remove(StringValues.UPDATED_BILL_DUE.value)
                labels_text.remove(StringValues.SECOND_TOUCH.value)
                if self.planning_commission:
                    labels_text.insert(0, StringValues.PLANNING_COMMISSION_MEETING.value)
                self.set_labels(labels_text)

                first_index = self.get_previous_two_meeting_indexes(agenda_bill_index, check_single=True)

                try:
                    first_touch = DATE_TIMES[first_index]
                    agenda_bill_due_date = first_touch - timedelta(days=6)
                except TypeError:
                    self.message_box_error(StringValues.REQUIRED_DATES_PAST.value)
                    return

                agenda_bill_final_update = selected_date - timedelta(days=6)

                no_public_hearing_values = [agenda_bill_due_date, first_touch, agenda_bill_final_update,
                                            selected_date]

                if self.planning_commission:
                    planning_commission_date = self.get_planning_commission_date(agenda_bill_due_date)
                    if planning_commission_date is None:
                        self.message_box_error(StringValues.REQUIRED_DATES_PAST.value)
                        return
                    no_public_hearing_values.insert(0, planning_commission_date)

                self.set_cell_values(no_public_hearing_values, [5] if self.planning_commission else [4, 5])

    def set_cell_values(self, values, blank_values):
        for index, item in enumerate(values):
            if item < datetime.now(tz=TIMEZONE):
                self.message_box_error(StringValues.REQUIRED_DATES_PAST.value)
                return
            self.grid.SetCellValue(index, 0, item.strftime(StringValues.RESULTS_TIME_OUTPUT_FORMAT.value))

        self.blank_row_colums_values(blank_values)

    def set_labels(self, labels_text):
        for index, text in enumerate(labels_text):
            self.grid.SetRowLabelValue(index, text)

    def blank_row_colums_values(self, index_list):
        for item in index_list:
            self.grid.SetRowLabelValue(item, "")
            self.grid.SetCellValue(item, 0, "")

    def message_box_error(self, message):
        wx.MessageBox(
            message=message,
            caption="Error",
            style=wx.OK | wx.ICON_ERROR,
        )
        self.blank_row_colums_values(range(6))

    @staticmethod
    def get_previous_two_meeting_indexes(selected_index, check_single=False):
        meeting_dates = []

        meeting_dates.extend(
            [index for index, date in enumerate(DATE_OPTIONS) if
             date.startswith(StringValues.MEETING_STRING.value)]
        )

        selected_meeting = meeting_dates.index(selected_index)

        # Check if required dates are available, if not, we return None to trip the TypeError
        if check_single:
            return None if selected_meeting < 1 else meeting_dates[selected_meeting - 1]
        else:
            return (None, None) if selected_meeting < 2 else (meeting_dates[selected_meeting - 2], meeting_dates[selected_meeting - 1])

    @staticmethod
    def get_planning_commission_date(initial_agenda_bill_due_date):
        for date_value in sorted(DATE_TIMES_OPTIONS, reverse=True):
            if date_value[0].startswith(StringValues.PLANNING_COMM_STRING.value) and date_value[1] < initial_agenda_bill_due_date:
                return date_value[1]


class SimpleGrid(wx.grid.Grid):
    def __init__(self, parent):
        wx.grid.Grid.__init__(self, parent, -1, pos=(300, 90), size=(425, 148))
        self.CreateGrid(6, 1)
        self.SetColSize(0, 200)
        self.SetRowLabelSize(225)
        self.SetColLabelValue(0, "Date")
        for row in range(6):
            self.SetRowLabelValue(row, "")


if __name__ == '__main__':
    app = wx.App()
    frm = AgendaFrame(None, title='Agenda Bill Due Date Calculator', id=-1, size=(800, 370))
    frm.Show()
    app.MainLoop()
